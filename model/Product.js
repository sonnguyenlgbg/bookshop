var mongoose = require('mongoose');
var Schema = mongoose.Schema;
mongoose.Promise = global.Promise;

var Product = new Schema({
  name 			:  String,
  unsignedName	: String,
  image 			: String,
  price 		: String,
  cateId 		: String,
  nameCate			: String,
  describe 			: String,
  author        : String
},{collection : 'product'});

module.exports = mongoose.model('Product', Product);