var mongoose = require('mongoose');
var Schema = mongoose.Schema;
mongoose.Promise = global.Promise;

var User = new Schema({
    name            : String,
    fullname 		: String,
    password 		: String,
    img 			: String,
    email 			: String,
    checkAdmin      : Boolean,

},{collection : 'user'});

module.exports = mongoose.model('User', User);