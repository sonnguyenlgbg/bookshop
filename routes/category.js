var express = require('express');
var router = express.Router();
var Cate = require('../model/Cate.js');

//Hàm bỏ dấu tiếng việt, mục đích tạo các đường link
function bodauTiengViet(str) {
    str = str.toLowerCase();
    str = str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, "a");
    str = str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, "e");
    str = str.replace(/ì|í|ị|ỉ|ĩ/g, "i");
    str = str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, "o");
    str = str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, "u");
    str = str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, "y");
    str = str.replace(/đ/g, "d");
    str = str.replace(/ /g, "-");
    str = str.replace(/\./g, "-");
    return str;
}

/* Trang chủ cate */
router.get('/', checkAdmin, function(req, res, next) {
  res.redirect('/user/cate/list-cate.html'); // Dẫn link tới trang danh sách danh mục
});

//Trang danh sách danh mục
router.get('/list-cate.html', checkAdmin,  function(req, res, next) {
	 Cate.find().then(function(cate){ //Truy vấn 
		res.render('admin/cate/listcate', {cate : cate});// Truyền đến trang, gửi kèm giữ liệu
	});
  	
});

// Trang thêm danh mục
router.get('/add-cate.html', checkAdmin, function(req, res, next) {
  res.render('admin/cate/addcate', { errors: null}); // Truyền đến trang 
});

//Trang thêm danh mục, post để nhận dữ liệu từ trang thêm danh mục gửi sang
router.post('/add-cate.html', checkAdmin,  function(req, res, next) {
  	req.checkBody('name', 'Giá Trị không được trống').notEmpty(); //Kiểm tra giá trị khi được nhập vào
 	req.checkBody('name', 'Tên từ 3 đến 30 ký tự').isLength({min:3, max:30});
	var errors = req.validationErrors();
	
	if (errors) {
		res.render('admin/cate/addcate',{errors : errors});  // truyền lỗi sang bên front-end nếu lỗi
	} else { 
		// Nếu không lỗi lấy giá trị
	var cate = new Cate({
		name 			: req.body.name,
		unsignedName 	: bodauTiengViet(req.body.name)
	});
	//Lưu vào csdl 
	cate.save().then(function(){
		req.flash('success_msg', 'Đã Thêm Thành Công'); // Gửi thông báo thành công
		res.redirect('add-cate.html'); //Load lại trang
	});
  }
});
//Trang edit danh mục
router.get('/:id/edit-cate.html', checkAdmin,  function(req, res, next) {
	Cate.findById(req.params.id, function(err, data){ //Tìm danh mục theo id
		res.render('admin/cate/editcate',{ errors: null, data: data}); //Load lại trang
	});	
});

//Trang edit danh mục, post để nhận dữ liệu từ trang edit danh mục gửi sang
router.post('/:id/edit-cate.html', checkAdmin,  function(req, res, next) {
	req.checkBody('name', 'Giá Trị không được trống').notEmpty(); // Kiểm tra giá trị nhập vào
  	req.checkBody('name', 'Name 3 đến 30 ký tự').isLength({min:3, max:30}); //Kiểm tra giá trị nhập vào
  	var errors = req.validationErrors();
  	if(errors){
  		Cate.findById(req.params.id, function(err, data){
			res.render('admin/cate/editcate',{ errors: errors, data: data}); // Gửi nếu bị lỗi
		});	
  	}else{
  		Cate.findById(req.params.id, function(err, data){ // Tìm sản phẩm theo id, và thêm sửa dữ liệu
			data.name 			= req.body.name;
			data.unsignedName 	= bodauTiengViet(req.body.name);
			data.save(); //lưu lại
			req.flash('success_msg', 'Đã Sửa Thành Công'); //Gửi thông báo
			// res.redirect('/user/cate/'+req.params.id+'/sua-cate.html');
			res.redirect('/user/cate');
		});
  	}

});
//Trang xóa sản phẩm
router.get('/:id/delete-cate.html',  checkAdmin,  function(req, res, next) {
	Cate.findById(req.params.id).remove(function() {  //Tìm sản phẩm theo id
		req.flash('success_msg', 'Đã Xoá Thành Công');
		res.redirect('/user/cate/list-cate.html');
	});
});

//Trang kiểm tra đăng nhập
function checkAdmin(req, res, next){
    if(req.isAuthenticated() && true == req.session.passport.user.checkAdmin){
      next();
    }else{
      res.redirect('/user/login.html');
    }

}
module.exports = router;