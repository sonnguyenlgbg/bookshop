
var express = require('express');
var router = express.Router();

var Cate = require('../model/Cate.js');
var Product = require('../model/Product.js');
var GioHang = require('../model/giohang.js');
var Cart = require('../model/Cart.js');
var User = require('../model/User.js');


var countJson = function(json){
	var count = 0;
	for(var id in json){
			count++;
	}

	return count;
}

/* GET home page. */
router.get('/', function (req, res) {
	// Data giỏ hàng
	var kiemtra = req.isAuthenticated();
	var giohang = new GioHang( (req.session.cart) ? req.session.cart : {items: {}} );
	var data = giohang.convertArray();

	Product.find().then(function(product){
		Cate.find().then(function(cate){ //Đây là 1 kiểu mà mongodb hỗ trợ để truy vấn, nó k phực tạp như môn sql mình đã học
			res.render('client/index',{product: product, cate: cate, data: data, kiemtra: kiemtra, title: 'Trang chủ book shop'});
		});
	});
});
//Trang thanh toán thành công
router.get('/paymentsuccess', function (req, res) {
	// Data giỏ hàng
	var kiemtra = req.isAuthenticated();
	var giohang = new GioHang( (req.session.cart) ? req.session.cart : {items: {}} );
	var data = giohang.convertArray();

	Product.find().then(function(product){
		Cate.find().then(function(cate){ //Đây là 1 kiểu mà mongodb hỗ trợ để truy vấn, nó k phực tạp như môn sql mình đã học
			res.render('client/paymentsuccess',{product: product, cate: cate, data: data, kiemtra: kiemtra, title: 'Trang chủ book shop'});
		});
	});
});
// tìm sản phẩm trang thanh toán thành công
router.post('/paymentsuccess', function (req, res) {
	var find = req.body.find;
	// var find = req.params.find;
	var kiemtra = req.isAuthenticated();
	var giohang = new GioHang( (req.session.cart) ? req.session.cart : {items: {}} );
	var data = giohang.convertArray();


	console.log(find);
	Cate.find().then(function(cate){
		Product.find({$text:{$search: find}}, function(err, result){
			res.render('./client/cate',{product: result, cate: cate, kiemtra: kiemtra,  title: 'Trang danh mục sản phẩm', data: data});
			console.log(result);
		});
	});
});
// tìm sản phẩm index
router.post('/', function (req, res) {
	var find = req.body.find;
	// var find = req.params.find;
	var kiemtra = req.isAuthenticated();
	var giohang = new GioHang( (req.session.cart) ? req.session.cart : {items: {}} );
	var data = giohang.convertArray();


	console.log(find);
	Cate.find().then(function(cate){
		Product.find({$text:{$search: find}}, function(err, result){
			res.render('./client/cate',{product: result, cate: cate, kiemtra: kiemtra,  title: 'Trang danh mục sản phẩm', data: data});
			console.log(result);
		});
	});
});
// // ?find=rewererwerwerrweer
// router.get('/?search/=:id', function (req, res) {
// 	// var find = req.body.find;
// 	var find1 = req.params.id;
// 	console.log(find1)
// 	Cate.find().then(function(cate){
// 		Product.find({$text:{$search: find1}}, function(err, result){
// 			res.render('/client/page/find',{product: result, cate: cate});
// 			console.log(result);
// 		});
// 	});
// });




router.get('/user', checkUser, function (req, res) {
	// Data giỏ hàng
	var kiemtra = req.isAuthenticated();
	var giohang = new GioHang( (req.session.cart) ? req.session.cart : {items: {}} );
	var data = giohang.convertArray();

	Product.find().then(function(product){
		Cate.find().then(function(cate){
			res.render('client/index',{product: product, cate: cate, data: data, kiemtra: kiemtra, title: 'Trang chủ book shop'});
		});
	});
   
});

// tìm sản phẩm user
router.post('/user', function (req, res) {
	var find = req.body.find;
	// var find = req.params.find;
	var kiemtra = req.isAuthenticated();
	var giohang = new GioHang( (req.session.cart) ? req.session.cart : {items: {}} );
	var data = giohang.convertArray();


	console.log(find);
	Cate.find().then(function(cate){
		Product.find({$text:{$search: find}}, function(err, result){
			res.render('./client/cate',{product: result, cate: cate, kiemtra: kiemtra,  title: 'Trang danh mục sản phẩm', data: data});
			console.log(result);
		});
	});
});

router.get('/cate/:name.:id.html', function (req, res) {
	// Data giỏ hàng
	var kiemtra = req.isAuthenticated();
	var giohang = new GioHang( (req.session.cart) ? req.session.cart : {items: {}} );
	var data = giohang.convertArray();

	Product.find({cateId: req.params.id}, function(err, product){
		Cate.find().then(function(cate){
			res.render('client/cate',{product: product, cate: cate, kiemtra: kiemtra, data: data, title: 'Trang danh mục sản phẩm'});
		});
	});
});

// // tìm sản phẩm cate
// router.post('/cate/:name.:id.html', function (req, res) {
// 	var find = req.body.find;
// 	var kiemtra = req.isAuthenticated();

// 	Cate.find().then(function(cate){
// 		Product.find({$text:{$search: find}}, function(err, result){
// 			res.render('client/page/cate.ejs',{product: result, cate: cate, kiemtra: kiemtra});
// 			console.log(result);
// 		});
// 	});
// });
// tìm sản phẩm cate
router.post('/cate/:name.:id.html', function (req, res) {
	var find = req.body.find;
	// var find = req.params.find;
	var kiemtra = req.isAuthenticated();
	var giohang = new GioHang( (req.session.cart) ? req.session.cart : {items: {}} );
	var data = giohang.convertArray();


	console.log(find);
	Cate.find().then(function(cate){
		Product.find({$text:{$search: find}}, function(err, result){
			res.render('./client/cate',{product: result, cate: cate, kiemtra: kiemtra,  title: 'Trang danh mục sản phẩm', data: data});
			console.log(result);
		});
	});
});


//Get cate
router.get('/cate', function (req, res) {
	// Data giỏ hàng
	var kiemtra = req.isAuthenticated();
	var giohang = new GioHang( (req.session.cart) ? req.session.cart : {items: {}} );
	var data = giohang.convertArray();

	Product.find().then(function(product){
		Cate.find().then(function(cate){
			res.render('client/cate',{product: product, cate: cate, data: data, kiemtra: kiemtra, title: 'Trang danh mục sản phẩm'});
		});
	});

});

// tìm sản phẩm cate
router.post('/cate', function (req, res) {
	var find = req.body.find;
	var kiemtra = req.isAuthenticated();
	var giohang = new GioHang( (req.session.cart) ? req.session.cart : {items: {}} );
	var data = giohang.convertArray();


	Cate.find().then(function(cate){
		Product.find({$text:{$search: find}}, function(err, result){
			res.render('client/cate',{product: result, cate: cate, kiemtra: kiemtra, data:data, title: 'Trang danh mục sản phẩm'});
			console.log(result);
		});
	});
});

// Giỏ hàng
router.get('/cart', function (req, res) {
	var giohang = new GioHang( (req.session.cart) ? req.session.cart : {items: {}} );
	var data = giohang.convertArray();
	var kiemtra = req.isAuthenticated();

		Cate.find().then(function(cate){
			res.render('client/cart',{ cate: cate, data: data, kiemtra: kiemtra, title: 'Trang giỏ hàng'});
		});

});


// // tìm sản phẩm cart

// router.post('/cart', function (req, res) {
// 	var find = req.body.find;
// 	var kiemtra = req.isAuthenticated();


// 	Cate.find().then(function(cate){
// 		Product.find({$text:{$search: find}}, function(err, result){
// 			res.render('client/cate',{product: result, cate: cate, kiemtra: kiemtra, title: 'Trang danh mục sản phẩm'});
// 			console.log(result);
// 		});
// 	});
// });

// tìm sản phẩm cate
router.post('/cart', function (req, res) {
	var find = req.body.find;
	var kiemtra = req.isAuthenticated();
	var giohang = new GioHang( (req.session.cart) ? req.session.cart : {items: {}} );
	var data = giohang.convertArray();


	Cate.find().then(function(cate){
		Product.find({$text:{$search: find}}, function(err, result){
			res.render('client/cate',{product: result, cate: cate, kiemtra: kiemtra, data:data, title: 'Trang danh mục sản phẩm'});
			console.log(result);
		});
	});
});


router.get('/detail/:name.:id.:cate.html',function (req, res) {
	var kiemtra = req.isAuthenticated();
	var giohang = new GioHang( (req.session.cart) ? req.session.cart : {items: {}} );
	var data = giohang.convertArray();

	Product.findById(req.params.id).then(function(product){
		Product.find({cateId: product.cateId, _id: {$ne: product._id}}).limit(4).then(function(pro){
			Cate.find().then(function(cate){
				res.render('client/product', {pro: pro, product: product, cate: cate, kiemtra: kiemtra, data: data, title: 'Trang chi tiết'});
			});
		});
	});
   
});

// tìm sản phẩm cate
router.post('/detail/:name.:id.:cate.html', function (req, res) {
	var find = req.body.find;
	var kiemtra = req.isAuthenticated();
	var giohang = new GioHang( (req.session.cart) ? req.session.cart : {items: {}} );
	var data = giohang.convertArray();


	Cate.find().then(function(cate){
		Product.find({$text:{$search: find}}, function(err, result){
			res.render('client/cate',{product: result, cate: cate, kiemtra: kiemtra, data:data, title: 'Trang danh mục sản phẩm'});
			console.log(result);
		});
	});
});


router.post('/order',checkUser, function (req, res) {
	
	req.checkBody('name', 'Họ tên không được trống').notEmpty();
	req.checkBody('diachi', 'Địa chỉ không được trống').notEmpty();
	req.checkBody('phone', 'Số điện thoại không được trống').notEmpty();
	req.checkBody('phone', 'Số điện thoại phải là số').isInt();
	var errors = req.validationErrors();
	var kiemtra = req.isAuthenticated();

	
	var giohang = new GioHang( (req.session.cart) ? req.session.cart : {items: {}} );
	var data = giohang.convertArray();
	var find = req.body.find;
	if (find != null) {
		Cate.find().then(function(cate){
			Product.find({$text:{$search: find}}, function(err, result){
				res.render('client/cate',{product: result, cate: cate, kiemtra: kiemtra, data:data, title: 'Trang danh mục sản phẩm'});
				console.log(result);
			});
		});
	} else if (errors) {
		
	Cate.find().then(function(cate){
		User.findById(req.session.passport.user._id).then(function(user){
			res.render('client/checkout', {errors: null,cate: cate, data: data, kiemtra: kiemtra, errors: errors, title: 'Trang thanh toán', user:user});
		});	
	});	
	} else {
	
	var cart = new Cart({
		  name 		: req.body.name,
		  email 	: req.body.email,
		  sdt 		: req.body.phone,
		  diachi	: req.body.diachi,
		  ghichu 	: req.body.ghichu,
		  cart 		: data,
		  st 		: 0
	});

	cart.save().then(function(){
		console.log("ok");
		req.session.cart = {items: {}};
		res.redirect('/paymentsuccess');
	});
}
	
});


router.get('/order',checkUser, function (req, res) {
	var giohang = new GioHang( (req.session.cart) ? req.session.cart : {items: {}} );
	var data = giohang.convertArray();
	var kiemtra = req.isAuthenticated();
	
	if(req.session.cart){
		if(countJson(req.session.cart.items) > 0){
			Cate.find().then(function(cate){
				User.findById(req.session.passport.user._id).then(function(user){
					res.render('client/checkout', {errors: null,cate: cate, data: data, kiemtra: kiemtra, title: 'Trang thanh toán', user, user});
				});	
			});	
		}else res.redirect('/');
		
	}else{
		res.redirect('/');
	}
});



router.post('/menu',function (req, res) {
 	Cate.find().then(function(data){
 		 res.json(data);
 	});
});

//Thêm sản phẩm
router.get('/add-cart.:id', function (req, res) {
	var id = req.params.id;

	var giohang = new GioHang( (req.session.cart) ? req.session.cart : {items: {}} ); //kiểm tra
	//kiểm tra xem có sản phẩm trong giỏ hàng có sản phẩm hay không
	
	
	Product.findById(id).then(function(data){
		
		giohang.add(id, data);
		req.session.cart = giohang;
		res.redirect('/cart');
	});
   
});



router.post('/updateCart', function (req, res) {
	var id 			= req.body.id;;
	var soluong 	= req.body.soluong;
	var giohang 	= new GioHang( (req.session.cart) ? req.session.cart : {items: {}} );

	giohang.updateCart(id, soluong);
	req.session.cart = giohang;
	res.json({st: 1});
	
});

router.post('/delCart', function (req, res) {
	var id 			= req.body.id;
	var giohang 	= new GioHang( (req.session.cart) ? req.session.cart : {items: {}} );

	giohang.delCart(id);
	req.session.cart = giohang;
	res.json({st: 1});
	
});

router.get('/logout.html', function (req, res) {
    req.logout();
    res.redirect('/');
});


function checkUser(req, res, next){
   
	if (req.isAuthenticated() && true == req.session.passport.user.checkAdmin){
		console.log(req.session.passport.user);
		res.render('admin/index');
		// next();
	} else if (req.isAuthenticated() && false == req.session.passport.user.checkAdmin) {
		console.log(req.session.passport.user);
		// res.render('client/index');
		next();
	} else {
		res.redirect('/user/login.html');
	
	}
}

module.exports = router;

