var express = require('express');
var router = express.Router();
var User = require('../model/User.js');
var bcrypt = require('bcryptjs');
var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;

/* GET add product page. */
router.get('/',checkAdmin, function(req, res, next) {
  if (req.isAuthenticated() && true == req.session.passport.user.checkAdmin){
      console.log(req.session.passport.user);
      res.render('admin/index');
  } else {
    res.render('client/cart');
  }

});

router.get('/login.html', function(req, res, next) {
  res.render('login/login', {title : 'Trang đăng nhập'});
});


router.post('/login.html',
  passport.authenticate('local', { successRedirect: '/user',
                                   failureRedirect: '/user/login.html',
                                   failureFlash: true })
);

passport.use(new LocalStrategy({
    usernameField: 'name',
    passwordField: 'password'
  },

  function(username, password, done) {
      User.findOne({name: username}, function(err, username){
          if(err) throw err;
          if(username){
            bcrypt.compare(password, username.password, function(err, user) {
                if(err) throw err;
                if(user){
                     return done(null, username);
                }else{
                   return done(null, false, { message: 'Tài khoản Không Đúng' });
                }
            });
          }else{
             return done(null, false, { message: 'Tài khoản Không Đúng' });
          }
      });
  }

));


passport.serializeUser(function(name, done) {

  done(null, name);
});


passport.deserializeUser(function(id, done) {
  User.findById(id, function(err, name) {
    done(err, name);
  });
});


router.post('/getUser',checkAdmin, function (req, res) {
    res.json(req.user);
});

router.get('/logout.html', function (req, res) {
    req.logout();
    console.log('OK đã log out')
    res.redirect('/user/login.html');
});


function checkAdmin(req, res, next){
   
    if(req.isAuthenticated()){
      next();
    }else{
      res.redirect('/user/login.html');
    }
}


module.exports = router;