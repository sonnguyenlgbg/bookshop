var express = require('express'); // Khai báo cáo modun cần dùng
var router = express.Router(); 
var Cart = require('../model/Cart.js'); 

// Trang chủ của giỏ hàng bên trong admin
router.get('/', checkAdmin, function(req, res, next) {
  res.redirect('/user/cart/list-cart.html');// chuyển trang mà mình muốn
});

//Trang danh sách giỏ hàng
router.get('/list-cart.html', checkAdmin, function(req, res, next) {
	Cart.find().then(function(data){ ///Câu lệnh truy vấn monodb, hỗ trợ sử dụng các biểu thức có sẵn 
		 res.render('admin/cart/listcart', {data: data});
	});
	
});

// Trang xem đơn hàng
router.get('/:id/view-cart.html', checkAdmin, function(req, res, next) {
 	var id = req.params.id;
 	Cart.findById(id).then(function(data){ // tìm đơn hàng dựa trên id
		 res.render('admin/cart/viewcart', {cart: data});
	});
});

//Trang Thanh toán
router.get('/:id/pay-cart.html',checkAdmin, function(req, res, next) {
 	var id = req.params.id;
 	Cart.findById(id, function(err, data){
 		data.st = 1; //st là status = 1 có nghĩ đã thanh toán, khi trưa thanh toán mặc định mình khai báo st = 0
 		data.save(); //Lưu database lại
 		req.flash('success_msg', 'Đã Thanh toán thành công'); // sau khi lưu thì gửi thông báo sang front-end
		res.redirect('/user/cart/'+id+'/view-cart.html');  // Lóa lại trang
 	});
});

//Trang xóa đơn hàng
router.get('/:id/delete-cart.html', checkAdmin, function(req, res, next) {
 	var id = req.params.id;
 	Cart.findOneAndRemove({_id: id}, function(err, offer){ //tìm đơn hàng theo id và xóa nó _id: là trường id trong csdl, id là mình từ link được truyền vào
 		req.flash('success_msg', 'Đã Xóa Thành Công');
		res.redirect('/user/cart/list-cart.html'); 
 	});
});

//Hàm check kiểm tra user là admin hay không
function checkAdmin(req, res, next){
	if(req.isAuthenticated() && true == req.session.passport.user.checkAdmin){ //req.isAuthenticated() có nghĩa xem đã được đăng nhập hay chưa,
		next();
	}else{
		res.redirect('/user/login.html');
	}
}



module.exports = router;