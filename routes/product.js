var express = require('express');
var router = express.Router();
var multer  = require('multer');

var Cate = require('../model/Cate.js');
var Product = require('../model/Product.js');

//Multer là một phần mềm trung gian node.js để xử lý multipart/form-data,
//  chủ yếu được sử dụng để tải lên các tệp. Nó được viết trên đầu busboy cho hiệu quả tối đa.

var storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, './public/upload'); //Lưu vào thư mục
  },
  filename: function (req, file, cb) {
    cb(null, Date.now() + "-" + file.originalname); //Tên file
  }
});
//Chỉ đến stirage
var upload = multer({ storage: storage });
 
//Bỏ dấu tiếng việt
function bodauTiengViet(str) {
    str = str.toLowerCase();
    str = str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, "a");
    str = str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, "e");
    str = str.replace(/ì|í|ị|ỉ|ĩ/g, "i");
    str = str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, "o");
    str = str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, "u");
    str = str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, "y");
    str = str.replace(/đ/g, "d");
    str = str.replace(/ /g, "-");
    str = str.replace(/\./g, "-");
    return str;
}

/* Trang chủ danh mục. */
router.get('/', checkAdmin, function (req, res) {
	res.redirect('/user/product/list-product.html')
});

router.get('/list-product.html', checkAdmin, function (req, res) {
	
	Product.find().then(function(pro){
	    Cate.find().then(function(cate){
		    res.render('admin/product/listproduct', {product: pro, cate: cate});
	    });
	});
});

router.get('/add-product.html', checkAdmin, function (req, res) {
	Cate.find().then(function(cate){
		res.render('admin/product/addproduct',{errors: null, cate: cate});
	});
});


router.post('/add-product.html', checkAdmin, upload.single('image'), function (req, res) {
	req.checkBody('name', 'Tên không được trống').notEmpty();
	req.checkBody('author', 'Tác giả không được trống').notEmpty();
	req.checkBody('price', 'Giá không được trống').notEmpty();

	// req.checkBody('des', 'Chi tiết không được rổng').notEmpty();
	console.log(req.file);
    var errors = req.validationErrors();
	if (errors) {
        console.log("Vao roi");
		var file = './public/upload/' + req.file.filename;
		  var fs = require('fs');
			fs.unlink(file, function(e){
				if(e) throw e;
			});
  		Cate.find().then(function(cate){
			res.render('admin/product/addproduct',{errors: errors, cate: cate});
		});
	}else{
		Cate.find({_id: req.body.cate}).then(function(data){
				
		console.log(JSON.stringify(data[0].unsignedName));
		var pro = new Product({
			name 			: req.body.name,
			unsignedName 	: bodauTiengViet(req.body.name),
			image 			: req.file.filename,
			cateId 			: req.body.cate,
			nameCate			: data[0].unsignedName,
			describe 			: req.body.describe,
			price 			: req.body.price,
			author 			: req.body.author,
			st 				: 0

		});

		pro.save().then(function(){
			req.flash('success_msg', 'Đã Thêm Thành Công');
			res.redirect('/user/product/add-product.html'); 
        });
		});
        
	}
	
});

router.get('/:id/edit-product.html', function (req, res) {
	Product.findById(req.params.id).then(function(data){
		Cate.find().then(function(cate){
			res.render('admin/product/editproduct',{errors: null, cate: cate, product: data});
		});
	});
	
});

router.post('/:id/edit-product.html',  upload.single('image'), function (req, res) {
	req.checkBody('name', 'Tên không được trống').notEmpty();
	req.checkBody('author', 'Tác giả không được trống').notEmpty();
	req.checkBody('price', 'Giá không được trống').notEmpty();
	// req.checkBody('des', 'Chi tiết không được trống').notEmpty();

    var errors = req.validationErrors();
	if (errors) {
		
		var file = './public/upload/' + req.file.filename;
		var fs = require('fs');
		fs.unlink(file, function(e){
			if(e) throw e;
		 });

  		Product.findById(req.params.id).then(function(data){
			Cate.find().then(function(cate){
				res.render('admin/product/editproduct',{errors: errors, cate: cate, product: data});
			});
		});
	}else{
		Product.findOne({ _id: req.params.id},  function(err, data){
			var file = './public/upload/' + data.image;
			var fs = require('fs');
			fs.unlink(file, function(e){
				if(e) throw e;
			 });
		Cate.find({_id: req.body.cate}).then(function(ca){

			data.name 					= req.body.name;
			data.unsignedName 	= bodauTiengViet(req.body.name);
			data.image 					= req.file.filename;
			data.cateId 				= req.body.cate;
			data.nameCate				= ca[0].unsignedName,
			data.describe 			= req.body.describe;
			data.price 					= req.body.price;
			data.author 					= req.body.author;
			data.st 			= '0';

			data.save();
				req.flash('success_msg', 'Đã Sửa Thành Công');
				res.redirect('/user/product/'+req.params.id+'/edit-product.html');
			//});


		});
	});

	}
	
});

router.get('/:id/delete-product.html', checkAdmin,  function (req, res) {


	Product.findById(req.params.id, function(err, data){
		var file = './public/upload/' + data.image;
		var fs = require('fs');
		fs.unlink(file, function(e){
			if(e) throw e;
		 });
		data.remove(function(){
			req.flash('success_msg', 'Đã Xoá Thành Công');
			res.redirect('/user/product/list-product.html');
		})
	});
	
});

function checkAdmin(req, res, next){
   
    if(req.isAuthenticated() && true == req.session.passport.user.checkAdmin){
      next();
    }else{
      res.redirect('/user/login.html');
    }
}

module.exports = router;
