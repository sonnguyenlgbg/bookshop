var express = require('express');
var router = express.Router();
var User = require('../model/User.js');
var bcrypt = require('bcryptjs');
var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;

router.get('/', function(req, res) {
  res.render('login/register', { message: req.flash('signupMessage'), title:'Trang đăng ký' });
});

// process the signup form
router.post('/', passport.authenticate('register', {
  successRedirect : '/register', // redirect to the secure profile section
  failureRedirect : '/register', // redirect back to the signup page if there is an error
  failureFlash : true // allow flash messages
}));

// used to serialize the user for the session
passport.serializeUser(function(user, done) {
    done(null, user.id);
});

// used to deserialize the user
passport.deserializeUser(function(id, done) {
    User.findById(id, function(err, user) {
        done(err, user);
    });
});

passport.use('register', new LocalStrategy({
  // by default, local strategy uses username and password, we will override with email
  usernameField : 'name',
  passwordField : 'password',
  passReqToCallback : true // allows us to pass in the req from our route (lets us check if a user is logged in or not)
},
function(req, name, password, done) {
  if (name)
    name = name.toLowerCase(); // Use lower-case e-mails to avoid case-sensitive e-mail matching

  // asynchronous
  process.nextTick(function() {
      // if the user is not already logged in:
      if (!req.user) {
          User.findOne({ 'name' :  name }, function(err, user) {
              // if there are any errors, return the error
              if (err)
                  return done(err);

              // check to see if theres already a user with that email
              if (user) {
                return done(null, false,  { message: 'danger' });
              } 
              
              else {

                  // create the user
                    var newUser  = new User();
                    newUser.name    = name;
                    newUser.fullname = req.param('fullname');
                    newUser.password = createHash(password);
                    newUser.img  = 'user.png';
                    newUser.email  = req.param('email');
                    newUser.checkAdmin = false;
                    console.log(newUser);
                    newUser.save(function(err) {
                    if (err)
                        return done(err);

                    return done(null,false, { message: 'success' });
                  });
              }

          });
      }
  });

}));

var createHash = function(password){
  return bcrypt.hashSync(password, bcrypt.genSaltSync(10), null);
}

module.exports = router;